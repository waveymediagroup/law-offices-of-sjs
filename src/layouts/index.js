import React, { Component, createContext } from 'react';
import PropTypes from 'prop-types';
import Link from 'gatsby-link';
import Helmet from 'react-helmet';
import styled from 'styled-components';

import Header from '../components/Header';
import Footer from '../components/Footer';

import '../assets/main.css';
import { media } from '../helpers';

const Main = styled.main`
	padding: 0;

	.page-wrap {
		padding-top: 156px;
		min-height: 100vh;
		${media.small`padding-top: 140px;`};
	}
`;

// const TemplateWrapper = ({ children, data }) => (

// );

class TemplateWrapper extends Component {
	constructor(props) {
		super(props);
		this.state = {
			spanish: true
		};

		this.updateLangSpanish = this.updateLangSpanish.bind(this);
		this.updateLangEnglish = this.updateLangEnglish.bind(this);
	}

	componentDidMount() {
		if (localStorage.getItem('spanish') == 'null') {
		} else {
			this.setState({
				spanish: true
			});
		}
	}

	updateLangSpanish(e) {
		e.preventDefault();
		localStorage.setItem('spanish', true);
		this.setState({
			spanish: true
		});
	}
	updateLangEnglish(e) {
		e.preventDefault();
		localStorage.setItem('spanish', null);
		this.setState({
			spanish: false
		});
	}

	render() {
		const { data, children } = this.props;
		const { spanish } = this.state;
		const updateLayoutFunction = this.updateLayoutFunction;
		const updateLangSpanish = this.updateLangSpanish;
		const updateLangEnglish = this.updateLangEnglish;
		const callText =
			spanish !== true
				? data.allWordpressAcfOptions.edges[0].node.call_now_text
				: data.allWordpressAcfOptions.edges[0].node.spanish_call_now_text;

		return (
			<div>
				<Main>
					<Helmet
						title="The Law Offices of Sergio J. Siderman, Esq."
						link={[
							{
								rel: 'stylesheet',
								href:
									'https://fonts.googleapis.com/css?family=Lato:300,400,700|Montserrat:300,400,500,700'
							},
							{
								rel: 'stylesheet',
								href: 'https://use.fontawesome.com/releases/v5.2.0/css/all.css'
							},
							{
								rel: 'stylesheet',
								href:
									'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css'
							},
							{
								rel: 'stylesheet',
								href:
									'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css'
							}
						]}
					>
						<meta name="google" content="notranslate" />
					</Helmet>
					<Header
						updateLangSpanish={updateLangSpanish}
						updateLangEnglish={updateLangEnglish}
						logo={data.logo}
						callText={callText}
						spanish={spanish}
					/>

					<div className="page-wrap">
						{this.props.children({
							...this.props,
							updateLayoutFunction,
							...this.state
						})}
					</div>

					<Footer />
				</Main>
			</div>
		);
	}
}

TemplateWrapper.propTypes = {
	children: PropTypes.func
};

export default TemplateWrapper;

export const query = graphql`
	query LayoutQuery {
		logo: imageSharp(id: { regex: "/dark-logo.png/" }) {
			sizes(quality: 100, maxWidth: 500) {
				...GatsbyImageSharpSizes
			}
		}

		allWordpressAcfOptions {
			edges {
				node {
					call_now_text
					spanish_call_now_text
				}
			}
		}
	}
`;
