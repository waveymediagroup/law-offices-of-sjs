import React from 'react';
import styled from 'styled-components';
import HalfBanner from '../components/HalfBanner';
import Wrapper from '../components/Wrapper';
import Img from 'gatsby-image';
import { SmallTitle } from '../components/Title';
import { TransitionMixin, media } from '../helpers';
import MetaTags from '../components/MetaTags';

const StyledSuccess = styled.div`
	.success-grid {
		> .inner-wrap {
			display: flex;
			flex-wrap: wrap;
			max-width: 90vw;
			margin: 0 auto;
			justify-content: center;

			> * {
				flex: 1 1 100%;
				margin: 0px 0px 20px;

				${media.small`flex: 1 1 40%;margin: 0px 20px 40px;`};
				${media.medium`flex: 1 1 25%;margin: 0px 20px 40px;`};
				${media.xl`flex: 1 1 30%;`};
			}

			&:after {
				content: ' ';
				flex: 1 1 100%;
			}
		}
	}
`;

const SuccessItemWrap = styled.div`
	border: 1px solid #efefef;
	max-width: 470px;
	margin: 0 auto 0px;
	${TransitionMixin('.25s')};

	&:hover {
		-webkit-box-shadow: 0 24px 38px 3px rgba(0, 0, 0, 0.14),
			0 9px 46px 8px rgba(0, 0, 0, 0.12), 0 11px 15px -7px rgba(0, 0, 0, 0.2);
		box-shadow: 0 24px 38px 3px rgba(0, 0, 0, 0.14),
			0 9px 46px 8px rgba(0, 0, 0, 0.12), 0 11px 15px -7px rgba(0, 0, 0, 0.2);
	}
	.header-wrap {
		position: relative;
		max-height: 500px;
		overflow: hidden;

		.gatsby-image-outer-wrapper {
			position: relative;
			max-height: 350px;
		}
	}

	.body-wrap {
		padding: 10px 20px;
		h3 {
			color: #222;
		}

		p {
			color: #777;
		}
	}
`;

const SuccessItem = ({ info, spanish }) => {
	const { title } = info.node;
	const { services, spanish_services } = info.node.acf;
	const featured_media = info.node.featured_media.localFile.childImageSharp;
	return (
		<SuccessItemWrap>
			<div className="header-wrap">
				<Img sizes={featured_media.sizes} />
			</div>
			{/* <div className="body-wrap">
				<h3>{title}</h3>
				<p>{spanish !== true ? services : spanish_services}</p>
			</div> */}
		</SuccessItemWrap>
	);
};

const SuccessStoriesPage = ({ data, spanish }) => {
	return (
		<StyledSuccess>
			<MetaTags
				title={
					spanish
						? 'Historias de éxito | The Law Offices of Sergio J. Siderman'
						: 'Success Stories | The Law Offices of Sergio J. Siderman'
				}
				description="Tenemos un largo historial de representación de inmigrantes y solicitantes de asilo en el sistema de justicia estadounidense. Nuestros abogados lo ayudarán a impugnar los cargos de los oficiales de Inmigración y Control de Aduanas (ICE) y utilizarán el sistema legal a su favor."
			/>
			<Wrapper>
				<SmallTitle>
					{spanish ? 'Historias de éxito' : 'Success Stories'}
				</SmallTitle>
			</Wrapper>

			<div className="success-grid">
				<div className="inner-wrap">
					{data.allWordpressWpSuccessStories.edges.map(item => {
						return <SuccessItem info={item} spanish={spanish} />;
					})}
				</div>
			</div>
		</StyledSuccess>
	);
};

export default SuccessStoriesPage;

export const query = graphql`
	query SuccessQuery {
		wordpressPage(title: { eq: "Success Stories" }) {
			title
			content
		}

		allWordpressWpSuccessStories {
			edges {
				node {
					id
					title
					featured_media {
						localFile {
							childImageSharp {
								sizes(quality: 100, maxWidth: 1200) {
									...GatsbyImageSharpSizes
								}
							}
						}
					}
					acf {
						services
						spanish_services
						brief_or_full_story
					}
				}
			}
		}
	}
`;
