import React, { Component } from 'react';
import Link from 'gatsby-link';
import Slider from 'react-slick';
import styled from 'styled-components';
import Img from 'gatsby-image';
import BtnClear from '../components/Elements/Btn';
import { SmallTitle } from '../components/Title';
import { ServiceGridItem } from '../components/GridItems/GridItems';
import { media, TransitionMixin } from '../helpers';
import MetaTags from '../components/MetaTags';

const BannerWrap = styled.section`
	position: relative;
	overflow: hidden;

	&:nth-of-type(3) {
		img:first-child {
		object-fit: contain !important;
		object-position: top !important;

		${media.medium`object-position: top !important;`};
	}
	}

	img {
		object-fit: contain !important;
		object-position: top !important;

		${media.medium`object-fit: cover !important;object-position: top !important;`};
	}
	.overlay {
		background: rgba(0, 0, 0, 0);
		position: absolute;
		height: 100%;
		width: 100%;
		top: 0;
		${media.medium`background: rgba(0, 0, 0, 0);`};
	}

	.gatsby-image-outer-wrapper {
		max-height: 250px;
		overflow: hidden;
		${media.small`max-height: 40vh;`};
		${media.medium`max-height: 70vh;`};
	}

	.gatsby-image-wrapper {
		height: 70vh;

		${media.medium`max-height: 70vh;`};
	}

	.content-wrap {
		position: absolute;
		top: 25%;
		text-align: center;
		width: 100%;
		display: none;

		${media.medium`top: 40%;`};

		h1 {
			color: #fff;
			font-size: 21px;
			font-weight: 400;
			font-family: 'Lato', sans-serif;
			margin: 0px auto;
			max-width: 1000px;
			padding: 0px 10px;
			${media.medium`font-size: 30px; padding: 0px;`};

			span {
				display: block;
				font-style: italic;
				font-size: 20px;
				margin-top: 5px;
				font-weight: 300;
			}
		}
	}

	.btn-wrap {
		padding-top: 20px;
	}

	&.slide-1 {
		img {
			${media.medium`object-position: 0px -120px!important;object-fit: cover;`};
		}
	}
	&.slide-2 {
		img {
			${media.medium`object-position: 0px -110px!important;object-fit: cover;`};
		}
	}
`;

const ServiceIntroWrap = styled.div`
	padding: 40px 0px;
`;

const ServiceGrid = styled.div`
	.inner-wrap {
		display: flex;
		flex-wrap: wrap;
		max-width: 80vw;
		margin: 0 auto;
		overflow-y: scroll;
		max-height: 100%;

		&:after {
			content: '';
			flex: 1 1 40%;
		}
	}

	.btn-wrap {
		padding-top: 0px;
		text-align: center;
	}
`;

class SimpleSlider extends React.Component {
	render() {
		var settings = {
			dots: false,
			arrows: false,
			infinite: true,
			speed: 500,
			autoplay: true,
			autoplaySpeed: 7000,
			fade: true,
			slidesToShow: 1,
			slidesToScroll: 1
		};

		let { one, two, three } = this.props;
		return (
			<Slider {...settings}>
				<div>
					<BannerWrap className="slide-1">
						<Img sizes={one.sizes} />
						<div className="overlay" />
					</BannerWrap>
				</div>
				<div>
					<BannerWrap className="slide-2">
						<Img
							sizes={two.sizes}
							imgStyle={{ objectPosition: '0px -360px' }}
						/>
						<div className="overlay" />
					</BannerWrap>
				</div>
				<div>
					<BannerWrap>
						<Img
							sizes={three.sizes}
							imgStyle={{ objectPosition: '0px -280px' }}
						/>
						<div className="overlay" />
					</BannerWrap>
				</div>
			</Slider>
		);
	}
}

const HelpSectionWrap = styled.div`
	background-color: #4c6696;
	background-image: url("data:image/svg+xml,%3Csvg width='30' height='30' viewBox='0 0 30 30' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M0 15l15 15H0V15zM15 0l15 15V0H15z' fill='%23ffffff' fill-opacity='0.02' fill-rule='evenodd'/%3E%3C/svg%3E");
	padding: 100px 0px;
	margin: 0px;
	.wrapper {
		text-align: center;

		.select-trigger {
			h2 {
				font-size: 32px;
				color: #fff;
				margin: 0px;
				margin-right: 20px;
				${media.medium`font-size: 50px;`};
			}

			p {
				color: #fff;
				max-width: 750px;
				margin: 10px auto 0px;
				font-weight: 300;
				word-spacing: 5px;
				font-size: 17px;
				line-height: 24px;
				padding: 0px 20px;

				${media.medium`font-size: 21px;line-height: 30px;padding: 0px;`};
			}
			.select-wrap {
				select {
					border: none;
					border-bottom: 4px solid #fff;
					background: transparent;
					font-size: 50px;
					color: #fff;
					font-family: 'Lato', sans-serif;
					border-radius: 0px !important;
					-webkit-appearance: none;
					-moz-appearance: none;
					text-indent: 1px;
					text-overflow: '';
					padding-bottom: 5px;
				}
			}
		}
	}
`;

const LogoGridWrap = styled.section`
	padding: 70px 0px;
	.title-wrap {
		text-align: center;
		h2 {
			font-size: 30px;
			font-weight: 300;
			margin: 0px;
		}
	}
	.logo-grid {
		max-width: 1200px;
		margin: 0 auto;
		display: flex;
		flex-wrap: wrap;
	}
`;

const LogoWrap = styled.div`
	flex: 1 1 100%;
	align-self: center;
	margin-top: 50px;
	${media.medium`flex: 1 1 25%;`};

	.gatsby-image-outer-wrapper {
		margin: 0 auto;
		max-width: 175px;
	}
`;

const SmallAboutWrap = styled.div`
	padding: 70px 0px;

	p {
		max-width: 950px;
		margin: 0px auto;
		font-size: 21px;
		text-align: center;
		font-weight: 300;
		font-size: 17px;
		line-height: 30px;
		padding: 0px 20px;
		${media.medium`font-size: 21px; line-height: 40px;`};
	}
`;

const SplitVideoWrap = styled.div`
	padding-bottom: 70px;
	.wrapper {
		max-width: 90vw;
		margin: 0 auto;
		${media.medium`display: flex; flex-wrap: wrap;`}

		.left-container {
			flex: 0 0 50%;
			margin-bottom: 20px;
			
			${media.medium`margin-right: 30px; margin-bottom: 0px;`}

			.iframe-container {
				margin-bottom: 20px;
			}

			a {
				color: blue;
			}

			.image-container {
				h3 {
					margin: 10px 0 20px;
				}
			}
		}
		.right-container {
			flex: 1;
			.iframe-container {
				margin-bottom: 20px;
			}
		}

		.center-container {
			flex: 1 1 100%;
			margin-top: 20px;

			iframe {
				display: block;
				max-width: 70%;
				margin: 0 auto 20px;
				border: 1px solid #ccc;
			}
		}

		.iframe-container {
			position: relative;
			overflow: hidden;
			padding-top: 56.25%;
			iframe {
			border: none;
			position: absolute;
			top: 0;
			left: 0;
			width: 100%;
			height: 100%;
			border: 0;
			}
			
		}

		


	}
`

export default class IndexPage extends Component {
	state = {
		selectActive: false
	};

	closeModal = () => {
		this.setState({
			selectActive: false
		});
	};
	render() {
		const { banner, wordpressPage } = this.props.data;
		const { data } = this.props;
		const { spanish } = this.props;
		const {
			banner_title,
			spanish_banner_title,
			banner_caption,
			spanish_banner_caption,
			services_title,
			spanish_services_title,
			spanish_services,
			logo_grid
		} = this.props.data.wordpressPage.acf;

		const {
			learn_more_text,
			spanish_learn_more_text,
			request_a_consultation_text,
			spanish_request_a_consultation_text
		} = this.props.data.allWordpressAcfOptions.edges[0].node;

		console.log(data)

		return (
			<div>
				<MetaTags
					title="The Law Offices of Sergio J. Siderman, Esq."
					description="En las Oficinas Legales de Sergio J. Siderman brindamos la representación legal de la más alta calidad para aquellos que buscan obtener la ciudadanía en los Estados Unidos."
				/>

				<SimpleSlider
					one={data.slideOne}
					two={data.slideTwo}
					three={data.slideThree}
				/>

				<SmallAboutWrap>
					<p>
						{spanish
							? 'En las Oficinas Legales de Sergio J. Siderman brindamos la representación legal de la más alta calidad para aquellos que buscan obtener la ciudadanía en los Estados Unidos. Nuestro equipo de abogados con experiencia en inmigración lo ayudará a completar las solicitudes de residencia (“Green Card”), resolver problemas con el Departamento de Seguridad Nacional y acceder a los servicios que usted y sus seres queridos necesitan para vivir el Sueño Americano.'
							: 'We at the Law Offices of Sergio J. Siderman provide the highest-quality legal representation for those seeking to obtain citizenship in the United States. Our team of experienced immigration lawyers will help you fill out green card applications, resolve issues with the Department of Homeland Security, and access the services you and your loved ones need to pursue the American Dream.'}
					</p>
				</SmallAboutWrap>

				<SplitVideoWrap>
					<div className="wrapper">
						
						<div className="left-container">
							<div className="iframe-container">
								<iframe width="560" height="315" src="https://www.youtube.com/embed/XLOiBZ2zcRQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
							</div>
							<div className="iframe-container">
								<iframe width="560" height="315" src="https://www.youtube.com/embed/n5kaFGAke6A" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
							</div>
						</div>
						
						<div className="right-container">
							<div className="iframe-container">
								<iframe width="560" height="315" src="https://www.youtube.com/embed/2b1M4lSsMF0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
							</div>
							<div className="iframe-container">
								<iframe width="560" height="315" src="https://www.youtube.com/embed/oeUfgCGq1y8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
							</div>
						</div>

						<div className="left-container">
							<a target="_blank" href="https://laopinion.com/2020/06/20/padre-nonagenario-de-ee-uu-da-ciudadania-a-hijo-nacido-en-mexico/">
								<div className="image-container">
									<Img sizes={data.padre.sizes} />
									<h3>Padre nonagenario de EE.UU. da ciudadanía a hijo nacido en México</h3>
								</div>
							</a>
						</div>

						<div className="center-container">
							<iframe width="100%" height="166" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/788467867&color=%23323333&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe>
							<iframe width="100%" height="166" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/788486704&color=%23323333&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe>
						</div>

					</div>
				</SplitVideoWrap>

				<HelpSectionWrap>
					<div className="wrapper">
						<HelpSection
							modalActive={this.state.selectActive == true ? true : false}
							closeModal={this.closeModal}
							lawOne={data.lawOne}
							lawTwo={data.lawTwo}
							lawThree={data.lawThree}
							lawFour={data.lawFour}
							spanish={spanish}
						/>
					</div>
				</HelpSectionWrap>

				{/* <LogoGridWrap>
					<div className="title-wrap">
						<h2>
							{spanish
								? 'Como nos vio y escuchó en:'
								: 'As seen, and heard on:'}
						</h2>
					</div>
					<div className="logo-grid inner-wrap">
						{logo_grid.map(logo => {
							console.log(logo.logo);
							return (
								<LogoWrap>
									<Img
										alt={
											logo.logo.title +
											' Logo | ' +
											'The Law Offices of Sergio Siderman '
										}
										sizes={logo.logo.localFile.childImageSharp.sizes}
									/>
								</LogoWrap>
							);
						})}
					</div>
				</LogoGridWrap> */}
			</div>
		);
	}
}

const HelpSectionGridWrap = styled.div`
	top: 0;
	z-index: 5000;
	/* background: rgba(42, 42, 42, 0.95); */
	opacity: 0;

	${TransitionMixin('.25s')};

	&.active {
		opacity: 1;
	}

	.inner-wrap {
		.initial-wrap {
			max-width: 90vw;
			margin: 0 auto;
			text-align: center;
			padding-top: 20px;
			${media.medium`max-width: 80vw;`};
			h1 {
				color: #fff;
				font-size: 50px;
				margin-top: 0px;
			}

			h2 {
				a {
					color: #fff;
					text-decoration: none;
					font-size: 35px;
					font-weight: 300;
				}
			}

			.btn-wrap {
				flex-wrap: wrap;
				${media.small`display: flex;`};
				button {
					flex: 1 1 40%;
					background: #fff;
					color: #2a2a2a;
					font-size: 21px;
					display: block;
					border: none;
					margin: 0 auto 30px;
					border-radius: 4px;
					text-decoration: none;
					padding: 0px 0px 20px;
					width: 100%;

					${TransitionMixin('.25s')};
					&:hover {
						background: #fff;
						cursor: pointer;
						color: #2a2a2a;
						transform: scale(1.02);
					}

					.gatsby-image-outer-wrapper {
						margin-bottom: 20px;
						margin-top: -10px;
					}

					.gatsby-image-wrapper {
						max-height: 140px;
						${media.medium`max-height: 250px;`};
					}
				}

				button:first-child,
				button:nth-child(3) {
					margin-right: 15px;
				}
			}
		}
	}
	.reset-wrap {
		button {
			width: 100%;
			max-width: 600px;
			margin: 0 auto;
			padding: 20px;
			background: transparent;
			color: #fff;
			font-size: 13px;
			text-transform: uppercase;
			font-weight: bold;
			letter-spacing: 1.5px;
			box-shadow: none;
			border: 1px solid #fff;
			${TransitionMixin('.25s')};

			&:hover {
				cursor: pointer;
				background: #fff;
				color: #2a2a2a;
				box-shadow: none;
			}
		}
	}

	.form-wrap {
		max-width: 600px;
		margin: 0 auto;

		.call-now-box-wrap {
			.call-now-box {
				padding: 20px;
				border: 1px solid #fff;
				text-align: left;
				color: #fff;
				font-size: 20px;

				h3 {
					line-height: 34px;
					text-align: center;
					margin: 0;
				}

				a {
					color: #fff;
					text-decoration: none;
					font-weight: 300;
					display: block;
				}
			}
		}
		button {
			width: 100%;
			max-width: 600px;
			margin: 0 auto;
			padding: 20px;
			background: transparent;
			color: #fff;
			font-size: 13px;
			text-transform: uppercase;
			font-weight: bold;
			letter-spacing: 1.5px;
			box-shadow: none;
			border: 1px solid #fff;
			${TransitionMixin('.25s')};

			&:hover {
				cursor: pointer;
				background: #fff;
				color: #2a2a2a;
				box-shadow: none;
			}
		}
		p {
			text-align: left;
		}
		label {
			text-align: left;
			color: #fff;
			text-transform: uppercase;
			letter-spacing: 1px;
			font-size: 12px;

			input,
			textarea {
				display: block;
				font-size: 21px;
				width: 100%;
				padding: 10px;
				margin-top: 4px;
			}
		}
	}
`;

class HelpSection extends Component {
	state = {
		titleText: '',
		captionText: '',
		hideButtons: false,
		hideForm: true,
		hideNumber: true
	};

	haveUsCallYou = (service, description, spanish = this.props.spanish) => {
		document.querySelector('#service-input').value = service;
		let title = spanish
			? 'Por favor danos un poco más de información'
			: 'Please give us a little more information';
		this.setState({
			titleText: title,
			captionText: description,
			hideButtons: true,
			hideForm: false
		});
	};

	resetPopup = () => {
		let title = this.props.spanish ? 'Necesito ayuda con' : 'I need help with';
		this.setState({
			titleText: title,
			captionText: '',
			hideButtons: false,
			hideForm: true,
			hideNumber: true
		});
	};

	render() {
		let {
			modalActive,
			lawOne,
			lawTwo,
			lawThree,
			lawFour,
			spanish
		} = this.props;
		let title = spanish ? 'Necesito ayuda con' : 'I need help with';
		return (
			<HelpSectionGridWrap className={modalActive ? 'active' : 'active'}>
				<div className="select-trigger">
					<h2>{this.state.titleText == '' ? title : this.state.titleText}</h2>
					<p>{this.state.captionText}</p>
					<div className="select-grid" />
				</div>
				<div className="inner-wrap">
					<div className="initial-wrap">
						{/* <h1>{this.state.titleText}</h1> */}

						<div
							className="btn-wrap"
							style={this.state.hideButtons ? { display: 'none' } : {}}
						>
							<button
								className="btn-white-outline"
								onClick={() =>
									this.haveUsCallYou(
										spanish ? 'Corte de inmigración' : 'Immigration Court',
										spanish
											? 'Traer a tus seres queridos a los Estados Unidos puede ser una tarea muy emocionante, pero muy difícil. Nuestro despacho de abogados se especializa en adquirir visas, tarjetas de residencia y otras formas de ciudadanía estadounidense, para que pueda concentrarse en lo que es importante: crear su nueva vida en los Estados Unidos.'
											: "Bringing your loved ones to the United States can be a very exciting, but very difficult task. Our lawfirm specializes in acquiring visas, green cards, and other forms of US Citizenship, so you can focus on what's important: creating your new life in the US."
									)
								}
							>
								<Img sizes={lawOne.sizes} />
								{spanish ? 'Corte de inmigracion' : 'Immigration Court'}
							</button>
							<button
								className="btn-white-outline"
								onClick={() =>
									this.haveUsCallYou(
										spanish ? 'Audiencias de expulsión' : 'Removal Hearings',
										spanish
											? 'Traer a sus seres queridos a los Estados Unidos puede ser una tarea muy emocionante, pero muy difícil. Nuestro bufete de abogados se especializa en adquirir visas, tarjetas de residencia y otras formas de ciudadanía de los Estados Unidos, para que pueda concentrarse en lo que es importante: crear su nueva vida en los Estados Unidos.'
											: 'We understand that deportation does not just affect the individual, but the family of the individual as well. We regularly assist our clients with appeals to the Board of Immigration Appeals, Immigration Court, and Ninth Circuit Court of Appeals.'
									)
								}
							>
								<Img
									sizes={lawTwo.sizes}
									imgStyle={{ objectPosition: '0px -20px' }}
								/>
								{spanish ? 'Audiencias de expulsión' : 'Removal Hearings'}
							</button>
							<button
								className="btn-white-outline"
								onClick={() =>
									this.haveUsCallYou(
										spanish ? 'Apariciones de ICE' : 'ICE Appearances',
										spanish
											? '¿Su ser querido ha sido detenido por ICE? Podemos ayudarlo a encontrarlos en nuestra base de datos y brindarle la información que necesita para que puedan ser liberados.'
											: 'Has your loved one been detained by ICE? We can help you find them in our database, and give you the information you need to help get them released.'
									)
								}
							>
								<Img sizes={lawThree.sizes} />
								{spanish ? 'Apariciones de ICE' : 'ICE Appearances'}
							</button>
							<button
								className="btn-white-outline"
								onClick={() =>
									this.haveUsCallYou(
										spanish ? 'Ciudadanía' : 'Citizenship',
										spanish
											? 'Traer a sus seres queridos a los Estados Unidos puede ser una tarea muy emocionante, pero muy difícil. Nuestro bufete de abogados se especializa en adquirir visas, tarjetas de residencia y otras formas de ciudadanía de los Estados Unidos, para que pueda concentrarse en lo que es importante: crear su nueva vida en los Estados Unidos.										'
											: "Bringing your loved ones to the United States can be a very exciting, but very difficult task. Our lawfirm specializes in acquiring visas, green cards, and other forms of United States Citizenship, so you can focus on what's important: creating your new life in the United States"
									)
								}
							>
								<Img sizes={lawFour.sizes} />
								{spanish ? 'Ciudadanía' : 'Citizenship'}
							</button>
							<button
								className="btn-white-outline"
								onClick={() =>
									this.haveUsCallYou(spanish ? 'Otro' : 'Other', '')
								}
								style={{ padding: '20px 0px' }}
							>
								<span>
									<i className="fa fa-star" />
								</span>{' '}
								{spanish ? 'Otro' : 'Other'}
							</button>
						</div>

						<h2 style={this.state.hideNumber ? { display: 'none' } : {}}>
							<a href="tel:1-888-240-9962">
								<span>(888) 240-9962</span>
							</a>
						</h2>

						<div
							className="form-wrap"
							style={this.state.hideForm ? { display: 'none' } : {}}
						>
							<div className="call-now-box-wrap">
								<div className="call-now-box">
									<h3>
										{spanish
											? 'Llame ahora para obtener una consulta gratuita'
											: 'Call now for a free consultation'}
										<a href="">(888) 240-9962</a>
										{spanish
											? ' o rellene el siguiente formulario.'
											: 'or fill out the form below.'}
									</h3>
								</div>
							</div>
							<form
								name="home-page-form"
								method="POST"
								action="/success"
								data-netlify="true"
								data-netlify-honeypot="bot-field"
							>
								<input name="bot-field" type="hidden" />
								<input type="hidden" name="form-name" value="home-page-form" />
								<p>
									<label>
										{spanish ? 'Necesito ayuda con:' : 'I need help with:'}
										<input
											type="text"
											name="I need help with"
											id="service-input"
										/>
									</label>
								</p>
								<p>
									<label>
										{spanish ? 'Nombre' : 'Name'}
										<input type="text" name="Name" id="service-input" />
									</label>
								</p>
								<p>
									<label>
										{spanish ? 'Su correo electrónico: ' : 'Your Email: '}
										<input type="email" name="Email" />
									</label>
								</p>
								<p>
									<label>
										{spanish ? 'Su número de teléfono:' : 'Your Phone Number:'}
										<input type="text" name="Phone" />
									</label>
								</p>

								<p>
									<label>
										{spanish
											? 'El mejor día de la semana para ser contactado:'
											: 'Best day of the week to be contacted:'}
										<input
											type="text"
											name="Best day of the week to be contacted"
										/>
									</label>
								</p>

								<p>
									<button type="submit">{spanish ? 'Enviar' : 'Submit'}</button>
								</p>
							</form>
						</div>

						<div className="reset-wrap">
							<button
								style={this.state.hideForm ? { display: 'none' } : {}}
								onClick={this.resetPopup}
							>
								{spanish ? 'Regresar' : 'Back'}
							</button>
							{/* <button onClick={closeModal}>close</button> */}
						</div>
					</div>
				</div>
			</HelpSectionGridWrap>
		);
	}
}

export const query = graphql`
	query HomeQuery {
		slideOne: imageSharp(id: { regex: "/court-slide-1.jpeg/" }) {
			sizes(quality: 100, maxWidth: 1920) {
				...GatsbyImageSharpSizes
			}
		}

		slideTwo: imageSharp(id: { regex: "/slide-2.jpeg/" }) {
			sizes(quality: 100, maxWidth: 1920) {
				...GatsbyImageSharpSizes
			}
		}

		slideThree: imageSharp(id: { regex: "/team.jpg/" }) {
			sizes(quality: 100, maxWidth: 1920) {
				...GatsbyImageSharpSizes
			}
		}

		lawOne: imageSharp(id: { regex: "/law-one.jpeg/" }) {
			sizes(quality: 100, maxWidth: 700) {
				...GatsbyImageSharpSizes
			}
		}
		lawTwo: imageSharp(id: { regex: "/bg-new.jpg/" }) {
			sizes(quality: 100, maxWidth: 1920) {
				...GatsbyImageSharpSizes
			}
		}
		lawThree: imageSharp(id: { regex: "/law-three.jpeg/" }) {
			sizes(quality: 100, maxWidth: 1920) {
				...GatsbyImageSharpSizes
			}
		}
		lawFour: imageSharp(id: { regex: "/law-four.jpeg/" }) {
			sizes(quality: 100, maxWidth: 1920) {
				...GatsbyImageSharpSizes
			}
		}
		padre: imageSharp(id: { regex: "/padre-2.jpg/" }) {
			sizes(quality: 100, maxWidth: 600) {
				...GatsbyImageSharpSizes
			}
		}

		wordpressPage(title: { eq: "Home" }) {
			title
			id
			acf {
				banner_title
				spanish_banner_title
				banner_caption
				spanish_banner_caption
				services_title
				spanish_services_title
				logo_grid {
					logo {
						title
						localFile {
							childImageSharp {
								sizes(quality: 100, maxWidth: 400) {
									...GatsbyImageSharpSizes
								}
							}
						}
					}
				}

				services {
					icon
					title
					body
				}
				spanish_services {
					icon
					title
					body
				}
			}
		}

		allWordpressAcfOptions {
			edges {
				node {
					learn_more_text
					spanish_learn_more_text
					request_a_consultation_text
					spanish_request_a_consultation_text
				}
			}
		}
	}
`;
