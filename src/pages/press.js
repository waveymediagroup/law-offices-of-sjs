import React from 'react';
import styled from 'styled-components';
import HalfBanner from '../components/HalfBanner';
import Wrapper from '../components/Wrapper';
import Img from 'gatsby-image';
import { SmallTitle } from '../components/Title';
import { media } from '../helpers';

const StyledPress = styled.div`
	padding-bottom: 50px;
	.form-wrap {
		.split-wrap {
			display: flex;
			flex-wrap: wrap;

			.location-section {
				flex: 1 1 100%;
				${media.medium`flex: 1 1 35%;`};
				> .inner-wrap {
					max-width: 85vw;
					margin: 0 auto;

					${media.medium`max-width: 60%;`};

					.location-wrap {
						margin-bottom: 30px;
						h2 {
							margin-top: 0px;
						}
						h3 {
							margin: 0px;
							font-size: 17px;
						}
						p {
							font-weight: 300;
							line-height: 25px;
							margin-top: 0px;
							margin-bottom: 5px;
							span {
								display: block;
							}
						}
						ul {
							list-style: none;
							text-align: left;
							margin: 0px;
							padding: 0px;

							li {
								display: inline-block;
								margin: 0px 10px 0px 0px;

								a {
									color: #222;
								}
							}
						}
					}
				}
			}
			.form-section {
				flex: 1 1 100%;
				margin-top: 50px;
				${media.medium`flex: 1 1 65%; margin-top: 0px;`};
				.inner-wrap {
					max-width: 85vw;
					margin: 0 auto;
					padding-bottom: 50px;
					${media.medium`max-width: 50vw;`};
				}

				h2 {
					margin: 0px 0px 20px;
					font-size: 21px;
				}
				input,
				label {
					display: block;
				}

				label {
					margin-bottom: 10px;
				}

				input {
					width: 100%;
					border-radius: 4px;
					margin-bottom: 15px;
				}

				input[type='submit'] {
					text-transform: uppercase;
					color: #fff;
					background: #000;
					padding: 20px 0px;
					width: 100%;
				}
			}
		}
	}
`;

export default ({ data, spanish }) => (
	<StyledPress>
		<Wrapper>
			<SmallTitle>{spanish !== true ? 'Press' : 'Noticias'}</SmallTitle>
		</Wrapper>

		<div className="press-grid">
			<div className="inner-wrap">
				<div className="press-item" />
			</div>
		</div>
	</StyledPress>
);

export const query = graphql`
	query PressQuery {
		wordpressPage(title: { eq: "Press" }) {
			title
			id
			content
			acf {
				press_items {
					press_title
					press_image {
						localFile {
							childImageSharp {
								sizes(quality: 100, maxWidth: 700) {
									...GatsbyImageSharpSizes
								}
							}
						}
					}
				}
			}
		}
	}
`;
