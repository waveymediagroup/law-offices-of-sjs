import React from 'react';
import styled from 'styled-components';
import HalfBanner from '../components/HalfBanner';
import Wrapper from '../components/Wrapper';
import Img from 'gatsby-image';
import { media } from '../helpers';
import MetaTags from '../components/MetaTags';
const StyledAbout = styled.div`
	padding-top: 40px;
	.content-row {
		.inner-wrap {
			display: flex;
			flex-wrap: wrap;
			margin: 0 auto;
			padding-bottom: 50px;
			${media.small`max-width: 90vw; flex-wrap: nowrap;`};
			.image-section {
				flex: 1 1 100%;
				align-self: center;
				${media.small`flex: 1 1 50%;`};
				.gatsby-image-outer-wrapper {
					margin: 0 auto;
					${media.small`max-width: 400px;`};
				}
			}

			.content-section {
				flex: 1 1 50%;
				align-self: center;
				padding: 10px 20px;
				h2 {
					margin-top: 20px;
					flex: 1 1 50%;
					text-align: left;
					${media.small`max-width: 400px;margin-top: 0px;`};
				}

				p {
					font-weight: 300;
					line-height: 30px;
					font-size: 15px;
					margin-bottom: 20px;

					&:last-child {
						margin-bottom: 0px;
					}
				}

				ul {
					li {
						line-height: 30px;
						font-size: 15px;
					}
				}
			}
		}
	}
	h2 {
		text-align: center;
		margin-top: 0px;
	}

	p {
		text-align: left;
	}
`;

const AboutPage = ({ data, spanish }) => {
	const { title, content } = data.wordpressPage;
	const {
		spanish_title,
		spanish_blurb,
		sergio_bio,
		spanish_sergio_bio,
		banner_image
	} = data.wordpressPage.acf;
	console.log(banner_image);

	return (
		<StyledAbout>
			<MetaTags
				title={
					spanish
						? spanish_title + ' | ' + 'The Law Offices of Sergio J. Siderman'
						: 'About Us | The Law Offices of Sergio J. Siderman'
				}
				description={spanish_blurb}
			/>
			<HalfBanner
				title="About"
				image={banner_image.localFile.childImageSharp}
			/>
			<Wrapper image={true}>
				<h2>{spanish != true ? title : spanish_title}</h2>
				<div
					dangerouslySetInnerHTML={{
						__html: `${spanish !== true ? content : spanish_blurb}`
					}}
				/>
			</Wrapper>

			<div className="content-row" id="ss-overview">
				<div className="inner-wrap">
					<div className="image-section">
						<Img sizes={data.headshot.sizes} />
					</div>
					<div className="content-section">
						<h2>Sergio J. Siderman</h2>
						<div
							dangerouslySetInnerHTML={{
								__html: `${spanish !== true ? sergio_bio : spanish_sergio_bio}`
							}}
						/>
					</div>
				</div>
			</div>
		</StyledAbout>
	);
};

export default AboutPage;

export const query = graphql`
	query AboutQuery {
		aboutBanner: imageSharp(id: { regex: "/team.jpg/" }) {
			sizes(quality: 100, maxWidth: 2100) {
				...GatsbyImageSharpSizes
			}
		}
		headshot: imageSharp(id: { regex: "/headshotNew.png/" }) {
			sizes(quality: 100, maxWidth: 1200) {
				...GatsbyImageSharpSizes
			}
		}

		wordpressPage(title: { eq: "About" }) {
			title
			content
			id

			acf {
				spanish_title
				spanish_blurb
				banner_image {
					localFile {
						childImageSharp {
							sizes(quality: 100, maxWidth: 1450) {
								...GatsbyImageSharpSizes
							}
						}
					}
				}
				sergio_pic {
					localFile {
						childImageSharp {
							sizes(quality: 100, maxWidth: 1200) {
								...GatsbyImageSharpSizes
							}
						}
					}
				}
				sergio_pic {
					id
					localFile {
						childImageSharp {
							sizes(quality: 100, maxWidth: 1200) {
								...GatsbyImageSharpSizes
							}
						}
					}
				}
				sergio_bio
				spanish_sergio_bio
			}
		}
	}
`;
