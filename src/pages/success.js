import React from 'react';
import styled from 'styled-components';
import HalfBanner from '../components/HalfBanner';
import Wrapper from '../components/Wrapper';
import Img from 'gatsby-image';
import { SmallTitle } from '../components/Title';
import { media } from '../helpers';
import MetaTags from '../components/MetaTags';

const StyledContactSuccess = styled.div`
	padding-bottom: 50px;
	.form-wrap {
		.split-wrap {
			display: flex;
			flex-wrap: wrap;

			.location-section {
				flex: 1 1 100%;
				${media.medium`flex: 1 1 35%;`};
				> .inner-wrap {
					max-width: 85vw;
					margin: 0 auto;

					${media.medium`max-width: 60%;`};

					.location-wrap {
						margin-bottom: 30px;
						h2 {
							margin-top: 0px;
						}
						h3 {
							margin: 0px;
							font-size: 17px;
						}
						p {
							font-weight: 300;
							line-height: 25px;
							margin-top: 0px;
							margin-bottom: 5px;
							span {
								display: block;
							}
						}
						ul {
							list-style: none;
							text-align: left;
							margin: 0px;
							padding: 0px;

							li {
								display: inline-block;
								margin: 0px 10px 0px 0px;

								a {
									color: #222;
								}
							}
						}
					}
				}
			}
			.form-section {
				flex: 1 1 100%;
				margin-top: 50px;
				${media.medium`flex: 1 1 65%; margin-top: 0px;`};
				.inner-wrap {
					max-width: 85vw;
					margin: 0 auto;
					padding-bottom: 50px;
					${media.medium`max-width: 50vw;`};
				}

				h2 {
					margin: 0px 0px 20px;
					font-size: 21px;
				}
				input,
				label {
					display: block;
				}

				label {
					margin-bottom: 10px;
				}

				input {
					width: 100%;
					border-radius: 4px;
					margin-bottom: 15px;
				}

				.submit-wrap {
					margin-top: 20px;
				}

				input[type='submit'] {
					text-transform: uppercase;
					color: #fff;
					background: #4c6696;
					padding: 20px 0px;
					width: 100%;
				}
			}
		}
	}
`;

export default ({ data, spanish }) => (
	<StyledContactSuccess>
		<MetaTags
			title={
				spanish !== true
					? 'Success Stories' + ' | ' + 'The Law Offices of Sergio J. Siderman'
					: 'Contáctenos' + ' | ' + 'The Law Offices of Sergio J. Siderman'
			}
			description="Nuestro personal brinda asesoría experta en solicitudes de residencia (“Green Card”) y documentos de ciudadanía. Lo ayudamos a procesar formularios que le permitirán obtener visas, permisos de trabajo y otros servicios proporcionados por la Ciudad de Los Ángeles y el Estado de California."
		/>

		<Wrapper>
			<SmallTitle>{spanish !== true ? 'Form Submitted' : 'Gracias'}</SmallTitle>
			<p>
				{spanish
					? 'Gracias por enviar este formulario. ¡Nos pondremos en contacto con usted en breve!'
					: 'Thank you for submitting this form. We will get back to you shortly!'}
			</p>
			{/* <div
				dangerouslySetInnerHTML={{
					__html: `${
						spanish !== true
							? data.wordpressPage.content
							: data.wordpressPage.acf.spanish_contact_blurb
					}`
				}}
			/> */}
		</Wrapper>
	</StyledContactSuccess>
);
