import React from 'react';
import styled from 'styled-components';
import HalfBanner from '../components/HalfBanner';
import Wrapper from '../components/Wrapper';
import Img from 'gatsby-image';
import { SmallTitle } from '../components/Title';
import { media } from '../helpers';
import MetaTags from '../components/MetaTags';

const StyledContact = styled.div`
	padding-bottom: 50px;
	.form-wrap {
		.split-wrap {
			display: flex;
			flex-wrap: wrap;

			.location-section {
				flex: 1 1 100%;
				${media.medium`flex: 1 1 35%;`};
				> .inner-wrap {
					max-width: 85vw;
					margin: 0 auto;
					display: flex;
					flex-wrap: wrap;
					${media.medium`max-width: 60%;`};
					> h2 {
						flex: 1 1 100%;
						margin-top: 0px;
					}
					.location-wrap {
						margin-bottom: 30px;
						flex: 1 1 100%;
						h2 {
							margin-top: 0px;
						}
						h3 {
							margin: 0px;
							font-size: 17px;
						}
						p {
							font-weight: 300;
							line-height: 25px;
							margin-top: 0px;
							margin-bottom: 5px;
							span {
								display: block;
							}
						}
						ul {
							list-style: none;
							text-align: left;
							margin: 0px;
							padding: 0px;

							li {
								display: inline-block;
								margin: 0px 10px 0px 0px;

								a {
									color: #222;
								}
							}
						}
					}
				}
			}
			.form-section {
				flex: 1 1 100%;
				margin-top: 50px;
				${media.medium`flex: 1 1 65%; margin-top: 0px;`};
				.inner-wrap {
					max-width: 85vw;
					margin: 0 auto;
					padding-bottom: 50px;
					${media.medium`max-width: 50vw;`};
				}

				h2 {
					margin: 0px 0px 20px;
					font-size: 21px;
				}
				input,
				label {
					display: block;
				}

				label {
					margin-bottom: 10px;
				}

				input {
					width: 100%;
					border-radius: 4px;
					margin-bottom: 15px;
				}

				span.submit-wrap {
					margin-top: 20px;
					display: block;
				}

				input[type='submit'] {
					text-transform: uppercase;
					color: #fff;
					background: #4c6696;
					padding: 20px 0px;
					width: 100%;
				}
			}
		}
	}
`;

export default ({ data, spanish }) => (
	<StyledContact>
		{/* <HalfBanner title="Contact" image={data.aboutBanner} /> */}
		<MetaTags
			title={
				spanish !== true
					? 'Contact Us' + ' | ' + 'The Law Offices of Sergio J. Siderman'
					: 'Contáctenos' + ' | ' + 'The Law Offices of Sergio J. Siderman'
			}
			description="Nuestro personal brinda asesoría experta en solicitudes de residencia (“Green Card”) y documentos de ciudadanía. Lo ayudamos a procesar formularios que le permitirán obtener visas, permisos de trabajo y otros servicios proporcionados por la Ciudad de Los Ángeles y el Estado de California."
		/>
		<Wrapper>
			<SmallTitle>{spanish !== true ? 'Contact Us' : 'Contáctenos'}</SmallTitle>
			{/* <div
				dangerouslySetInnerHTML={{
					__html: `${
						spanish !== true
							? data.wordpressPage.content
							: data.wordpressPage.acf.spanish_contact_blurb
					}`
				}}
			/> */}
		</Wrapper>

		<div className="form-wrap">
			<div className="split-wrap">
				<div className="location-section">
					<div className="inner-wrap">
						<h2>{spanish ? 'Nuestras Oficinas' : 'Our Offices'}</h2>
						<div className="location-wrap">
							<h3>Los Angeles</h3>
							<p>
								1625 W. Olympic Blvd. <span>Suite 915,</span> Los Angeles, CA
								90015
							</p>
							<div className="links-wrap">
								<ul>
									{/* <li>
										<a href="#">
											{spanish !== true
												? 'Call'
												: data.allWordpressAcfOptions.edges[0].node
														.spanish_call_text}
										</a>
									</li>
									<li>|</li> */}
									<li>
										<a
											target="_blank"
											href="https://www.google.com/maps/place/The+Law+Offices+of+Sergio+Siderman/@34.04995,-118.2748563,15z/data=!4m2!3m1!1s0x0:0xc1642b1c7c6b1ff8?ved=2ahUKEwiz8MDou-3fAhVLh-AKHZClDBIQ_BIwCnoECAYQCA"
										>
											{spanish !== true
												? 'Directions'
												: data.allWordpressAcfOptions.edges[0].node
														.spanish_directions_text}
										</a>
									</li>
								</ul>
							</div>
						</div>

						<div className="location-wrap">
							<h3>San Bernardino</h3>
							<p>
								202 E. Airport Dr.
								<span>Suite 170,</span> San Bernardino, CA 92408
							</p>
							<div className="links-wrap">
								<ul>
									{/* <li>
										<a href="#">
											{spanish !== true
												? 'Call'
												: data.allWordpressAcfOptions.edges[0].node
														.spanish_call_text}
										</a>
									</li>
									<li>|</li> */}
									<li>
										<a
											target="_blank"
											href="https://www.google.com/maps/place/202+E+Airport+Dr+%23170,+San+Bernardino,+CA+92408/@34.0676731,-117.285874,17z/data=!3m1!4b1!4m5!3m4!1s0x80dcac91be0d962b:0x18132b0fb04a26ce!8m2!3d34.0676731!4d-117.2836853"
										>
											{spanish !== true
												? 'Directions'
												: data.allWordpressAcfOptions.edges[0].node
														.spanish_directions_text}
										</a>
									</li>
								</ul>
							</div>
						</div>

						<div className="location-wrap">
							<h3>San Diego</h3>
							<p>
								1455 Frazee Rd
								<span>Suite 503/504,</span> San Diego, CA 92108
							</p>
							<div className="links-wrap">
								<ul>
									{/* <li>
										<a href="#">
											{spanish !== true
												? 'Call'
												: data.allWordpressAcfOptions.edges[0].node
														.spanish_call_text}
										</a>
									</li>
									<li>|</li> */}
									<li>
										<a
											target="_blank"
											href="https://www.google.com/maps/place/1455+Frazee+Rd+Suite+503%2F504,+San+Diego,+CA+92108/@32.7744593,-117.1606487,17z/data=!3m1!4b1!4m5!3m4!1s0x80d9553910ddff21:0x8b992d50f97c4e1e!8m2!3d32.7744593!4d-117.15846"
										>
											{spanish !== true
												? 'Directions'
												: data.allWordpressAcfOptions.edges[0].node
														.spanish_directions_text}
										</a>
									</li>
								</ul>
							</div>
						</div>

						<div className="location-wrap">
							<h3>Van Nuys</h3>
							<p>
								16501 Sherman Way
								<span>Suite 101,</span> Van Nuys, CA 91406
							</p>
							<div className="links-wrap">
								<ul>
									{/* <li>
										<a href="#">
											{spanish !== true
												? 'Call'
												: data.allWordpressAcfOptions.edges[0].node
														.spanish_call_text}
										</a>
									</li>
									<li>|</li> */}
									<li>
										<a
											target="_blank"
											href="https://www.google.com/maps/place/16501+Sherman+Way+%23101,+Lake+Balboa,+CA+91406/data=!4m2!3m1!1s0x80c299fb644948b5:0x538103c1eec0c47c?ved=2ahUKEwimw5zXve3fAhVnT98KHQpwDm8Q8gEwAHoECAAQAQ"
										>
											{spanish !== true
												? 'Directions'
												: data.allWordpressAcfOptions.edges[0].node
														.spanish_directions_text}
										</a>
									</li>
								</ul>
							</div>
						</div>
						<div className="location-wrap">
							<h3>Las Vegas</h3>
							<p>
								4445 S Eastern Ave, <span>Suite B,</span> Las Vegas, NV 89119
							</p>
							<div className="links-wrap">
								<ul>
									{/* <li>
										<a href="#">
											{spanish !== true
												? 'Call'
												: data.allWordpressAcfOptions.edges[0].node
														.spanish_call_text}
										</a>
									</li>
									<li>|</li> */}
									<li>
										<a
											target="_blank"
											href="https://www.google.com/maps/place/4445+S+Eastern+Ave,+Las+Vegas,+NV+89119/@36.1085157,-115.1218535,17z/data=!3m1!4b1!4m5!3m4!1s0x80c8c518c3e63489:0x4009752949da6563!8m2!3d36.1085157!4d-115.1196648"
										>
											{spanish !== true
												? 'Directions'
												: data.allWordpressAcfOptions.edges[0].node
														.spanish_directions_text}
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div className="form-section">
					<div className="inner-wrap">
						<h2>
							{spanish !== true
								? data.wordpressPage.acf.form_title
								: data.wordpressPage.acf.spanish_form_title}
						</h2>
						<form
							name="contact-page"
							method="POST"
							action="/success"
							data-netlify="true"
							data-netlify-honeypot="bot-field"
							className="form"
						>
							<input name="bot-field" type="hidden" />
							<input type="hidden" name="form-name" value="contact-page" />
							<label htmlFor="name">
								{spanish ? 'Nombre' : 'Name'}
								<input
									type="text"
									className="form-control"
									name="name"
									required
								/>
							</label>
							<label htmlFor="email">
								{spanish ? 'Correo Electronico' : 'Email'}
								<input type="email" className="form-control" name="email" />
							</label>
							<label htmlFor="phone">
								{spanish ? 'Telefono' : 'Phone'}
								<input
									type="text"
									className="form-control"
									name="phone"
									required
								/>
							</label>
							<label htmlFor="inquiry">
								{spanish ? 'Mensaje' : 'Inquiry'}
								<textarea
									name="inquiry"
									className="form-control"
									cols="30"
									rows="10"
								/>
							</label>

							<input
								type="submit"
								className="btn-blue"
								value={spanish ? 'Enviar' : 'Submit'}
							/>
						</form>
					</div>
				</div>
			</div>
		</div>
	</StyledContact>
);

export const query = graphql`
	query ContactQuery {
		aboutBanner: imageSharp(id: { regex: "/contact.jpeg/" }) {
			sizes(quality: 100, maxWidth: 2100) {
				...GatsbyImageSharpSizes
			}
		}
		headshot: imageSharp(id: { regex: "/headshotNew.png/" }) {
			sizes(quality: 100, maxWidth: 1200) {
				...GatsbyImageSharpSizes
			}
		}

		wordpressPage(title: { eq: "Contact" }) {
			title
			id
			content
			acf {
				spanish_contact_blurb
				form_title
				spanish_form_title
			}
		}

		allWordpressAcfOptions {
			edges {
				node {
					spanish_call_text
					spanish_directions_text
				}
			}
		}
	}
`;
