import React from 'react';
import styled from 'styled-components';
import HalfBanner from '../components/HalfBanner';
import Wrapper from '../components/Wrapper';
import { Title, SmallTitle } from '../components/Title';
import Img from 'gatsby-image';
import { ContentRow } from '../components/GridItems/GridItems';
import MetaTags from '../components/MetaTags';
export default class ServicesPage extends React.Component {
	render() {
		const { data, children, spanish } = this.props;
		const {
			services_title,
			spanish_services_title,
			services_blurb,
			spanish_services_blurb
		} = this.props.data.wordpressPage.acf;

		return (
			<div>
				{/* <HalfBanner image={data.servicesBanner} /> */}
				<MetaTags
					title={
						spanish_services_title +
						' | ' +
						'The Law Offices of Sergio J. Siderman'
					}
					description="Nuestro equipo de abogados con experiencia en inmigración lo ayudará a completar las solicitudes de la tarjeta verde de residencia (“Green Card”), resolver problemas con el Departamento de Seguridad Nacional y acceder a los servicios que usted y sus seres queridos necesitan para perseguir el Sueño Americano."
				/>
				<Wrapper>
					<SmallTitle>
						{spanish !== true ? services_title : spanish_services_title}
					</SmallTitle>
					<div
						dangerouslySetInnerHTML={{
							__html: `${
								spanish !== true ? services_blurb : spanish_services_blurb
							}`
						}}
					/>
				</Wrapper>

				{data.wordpressPage.acf.content_rows.map((row, i) => {
					return <ContentRow key={i} info={row} spanish={spanish} />;
				})}
			</div>
		);
	}
}

export const query = graphql`
	query ServicesQuery {
		servicesBanner: imageSharp(id: { regex: "/servicesNew.jpeg/" }) {
			sizes(quality: 100, maxWidth: 2100) {
				...GatsbyImageSharpSizes
			}
		}
		lawOne: imageSharp(id: { regex: "/law-1.jpeg/" }) {
			sizes(quality: 100, maxWidth: 1200) {
				...GatsbyImageSharpSizes
			}
		}
		lawTwo: imageSharp(id: { regex: "/law-21.jpeg/" }) {
			sizes(quality: 100, maxWidth: 1200) {
				...GatsbyImageSharpSizes
			}
		}
		lawThree: imageSharp(id: { regex: "/law-3.jpeg/" }) {
			sizes(quality: 100, maxWidth: 1200) {
				...GatsbyImageSharpSizes
			}
		}

		wordpressPage(title: { eq: "Services" }) {
			title
			id
			acf {
				services_title
				spanish_services_title
				services_blurb
				spanish_services_blurb

				content_rows {
					content_row_image {
						localFile {
							childImageSharp {
								sizes(quality: 100, maxWidth: 2100) {
									...GatsbyImageSharpSizes
								}
							}
						}
					}
					content_row_title
					spanish_content_row_title
					content_row_body
					spanish_content_row_body
					flipped
				}
			}
		}
	}
`;
