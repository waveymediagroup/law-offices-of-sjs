import React, { Component } from 'react';
import Helmet from 'react-helmet';

export default class MetaTags extends Component {
	render() {
		const { title, description } = this.props;
		return (
			<Helmet>
				{/* title */}
				{title && <title>{title}</title>}
				{description && <meta name="description" content={description} />}
			</Helmet>
		);
	}
}
