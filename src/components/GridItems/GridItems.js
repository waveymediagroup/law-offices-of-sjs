import React, { Component } from 'react';
import styled from 'styled-components';
import { TransitionMixin, media } from '../../helpers';
import Img from 'gatsby-image';

const SplitGridWrap = styled.div`
	> .inner-wrap {
		max-width: 90vw;
		margin: 0 auto 40px;
		padding: 70px 0px;
		min-height: 80vh;
		${media.large`margin: 0 auto ;`};
		> * {
			flex: 1 1 50%;

			${media.large`padding: 0px 50px;`};
		}

		&:first-child {
			border-top: 1px solid #efefef;
		}

		${media.large`display: flex;`};

		.section {
			align-self: center;
			margin-bottom: 70px;
			${media.large`margin-bottom: 0px;min-height: 335px;`};
			.title-wrap {
				text-align: center;
				margin-bottom: 30px;

				i {
					font-size: 80px;

					&.fa-building {
						padding-left: 5px;
					}
				}

				h4 {
					margin-bottom: 0px;
				}
			}
			.body-wrap {
				margin-bottom: 25px;
				line-height: 25px;
				text-align: left;

				p {
					font-size: 14px;
					margin-bottom: 20px;
					font-weight: 300;
					${media.large`font-size: 16px;`};
					span {
						font-weight: bold;
					}
				}
			}
		}

		&.fw {
			max-width: 80vw;
			border-left: 1px solid #efefef;
			border-right: 1px solid #efefef;
			padding: 0px;
			min-height: 0;

			> * {
				padding: 0px;
			}

			.image-section {
				.gatsby-image-outer-wrapper {
					max-height: 600px;
					overflow: hidden;
				}
			}

			.content-section {
				align-self: center;

				> .inner-wrap {
					padding: 40px 20px;
					${media.large`max-width: 55vw; padding: 0 70px;`};
				}
				.title-wrap {
					margin-bottom: 25px;
					h1 {
						margin-top: 0px;
						margin-bottom: 0px;
						font-weight: 400;
						font-size: 18px;
						${media.large`font-size: 21px;`};
					}
				}
				.body-wrap {
					p {
						margin-bottom: 20px;
						line-height: 25px;
						color: #777;
						font-size: 14px;
						font-weight: 300;

						&:last-child {
							margin-bottom: 0px;
						}
					}
				}
			}
		}

		&.flipped {
			.image-section {
				order: 1;
				.gatsby-image-outer-wrapper {
					overflow: hidden;
					${media.laptop`max-height: 420px;`};

					.gatsby-image-wrapper {
						${media.large`padding: 200px;`};
					}
				}
			}
			.content-section {
				.inner-wrap {
					${media.large`padding: 100px 70px;`};
				}
			}
		}

		&.last {
			.gatsby-image-wrapper {
				padding: 100px;
			}
			.content-section {
				.inner-wrap {
					${media.large`padding: 100px 70px;`};
				}
			}
		}
	}
`;

const Item = styled.div`
	flex: 1 1 100%;
	padding: 30px 0px;
	${media.medium`flex: 1 1 40%; padding: 40px;`};
	.title {
		h4 {
			margin-top: 0px;
		}
	}

	p {
		font-size: 15px;
		line-height: 25px;
	}
`;

// Home Page
export class ServiceGridItem extends Component {
	render() {
		const { title, body, icon } = this.props.service;
		return (
			<Item>
				<div className="title">
					<h4>
						<span>
							<i className="fa fa-adjust" />
						</span>{' '}
						{title}{' '}
					</h4>
				</div>
				<div className="body">
					<p>{body}</p>
				</div>
			</Item>
		);
	}
}

// Service Content Row

export class ContentRow extends Component {
	render() {
		const { spanish } = this.props;
		const {
			content_row_title,
			spanish_content_row_title,
			content_row_body,
			spanish_content_row_body,
			flipped
		} = this.props.info;
		const {
			sizes
		} = this.props.info.content_row_image.localFile.childImageSharp;
		return (
			<SplitGridWrap>
				<div
					className={
						flipped == 'flipped' ? 'inner-wrap fw flipped' : 'inner-wrap fw'
					}
				>
					<div className="image-section">
						<Img sizes={sizes} />
					</div>
					<div className="content-section">
						<div className="inner-wrap">
							<div className="title-wrap lg">
								<h1>
									{spanish !== true
										? content_row_title
										: spanish_content_row_title}
								</h1>
							</div>
							<div className="body-wrap">
								<p>
									{spanish !== true
										? content_row_body
										: spanish_content_row_body}
								</p>
							</div>
						</div>
					</div>
				</div>
			</SplitGridWrap>
		);
	}
}
