import React, { Component } from 'react';
import Img from 'gatsby-image';
import styled from 'styled-components';
import { TransitionMixin, media } from '../helpers';

const HalfBannerWrap = styled.div`
	position: relative;
	.gatsby-image-outer-wrapper {
		position: relative;
		max-height: 400px;
		min-height: 300px;
		overflow: hidden;

		background-color: #fff;
		margin: 0 auto;
		${media.medium`max-width: 70%;`};
		.gatsby-image-wrapper {
			min-height: 300px;
		}
	}
	.overlay {
		height: 100%;
		width: 100%;
		position: absolute;
		background: rgba(0, 0, 0, 0.4);
		top: 0;
	}

	.content-wrap {
		position: absolute;
		top: 45%;
		width: 100%;
		text-align: center;

		h1 {
			color: #fff;
			margin: 0px;
			font-size: 40px;
			${media.large`font-size: 60px;`};
		}
	}
`;

export default class HalfBanner extends Component {
	render() {
		const { image, title } = this.props;
		return (
			<HalfBannerWrap>
				<Img sizes={image.sizes} />
				{/* <div className="overlay" /> */}
				{/* <div className="content-wrap">
					<h1>{title}</h1>
				</div> */}
			</HalfBannerWrap>
		);
	}
}
