import React from 'react';
import styled from 'styled-components';
import Link from 'gatsby-link';
import Img from 'gatsby-image';
import { media, TransitionMixin } from '../../helpers';
import MenuLinks from '../Elements/Links';
const StyledHeader = styled.div`
	position: fixed;
	width: 100%;
	z-index: 2000;
	top: 0px;
	border-bottom: 1px solid #efefef;
	${media.small`position: absolute;`};
	.top-bar-wrap {
		padding: 0px 0px;
		background: #4c6696;
		border-bottom: 1px solid #fff;
		position: relative;
		z-index: 10;
		> .inner-wrap {
			display: flex;
			flex-wrap: wrap;

			${media.small`flex-wrap: nowrap;`};
		}

		.number {
			align-self: center;
			justify-self: center;
			flex: 1;
			.number-wrap {
				padding: 15px 0px;
				border: 1px solid #fff;
				${media.small`float: right; margin-right: 40px; border: none;`};
				h3 {
					margin: 0px;
					text-align: center;
					color: #fff;
					font-weight: 400;
					font-size: 13px;

					${media.small`font-size: 17px;`};

					a {
						color: #fff;
						text-decoration: none;
					}
					span {
						margin-left: 10px;
					}
				}
			}
		}

		.language-select {
			display: flex;
			flex: 1 1 100%;
			${media.small`max-width: 400px;`};
			> * {
				flex: 1 1 50%;
			}

			.sm-button-wrap {
				a {
					margin-top: 1px;
					&.active {
						background: #fff;
						color: #4c6696;
						border-bottom: 1px solid;
					}
				}
				&:first-child {
					a {
						border-right: 0px;
					}
				}
			}

			a {
				color: #fff;
				text-decoration: none;
				padding: 16px 0px;
				border-left: 1px solid #efefef;
				border-right: 1px solid #efefef;
				display: inline-block;
				font-size: 14px;
				width: 100%;
				text-align: center;
				letter-spacing: 1px;
				${media.small`width: 200px; font-size: 16px;`};
			}
		}
	}
	.primary-header-wrap {
		background: #fff;
		padding: 15px 0px;
		position: relative;
		z-index: 10;
		.inner-wrap {
			max-width: 90vw;
			margin: 0 auto;
			display: flex;

			> * {
				flex: 1 1 10%;
			}
			.logo-section {
				align-self: center;
				max-width: 400px;

				${media.xl`max-width: 100%;`};
				a {
					text-decoration: none;
				}
				h1 {
					color: #fff;
					font-weight: 300;
				}

				.gatsby-image-outer-wrapper {
					max-width: 350px;
					display: inline-block;
					width: 100%;
				}
			}

			.link-section {
				text-align: right;
				align-self: center;

				.mobile-wrap {
					${media.large`display: none;`};
				}

				.desktop-links {
					display: none;

					${media.large`display: block;`};
					ul {
						margin: 0px;
						padding: 0px;

						li {
							display: inline-block;
							margin: 0px 10px;
							a {
								color: #222;
								text-transform: uppercase;
								text-decoration: none;
								font-size: 14px;
							}
						}
					}
				}
			}
		}
	}
`;

const MobileMenuWrap = styled.div`
	background: rgba(34, 34, 34, 0.9);
	height: 100vh;
	position: fixed;
	width: 100%;
	transform: translateY(-150%);
	${TransitionMixin('.25s')};

	&.active {
		transform: translateY(0%);
	}

	.mobile-link-wrap {
		ul {
			list-style: none;
			padding-left: 20px;
			li {
				a {
					color: #fff;
					display: block;
					padding: 10px;
					text-transform: uppercase;
					text-decoration: none;
				}
			}
		}
	}
`;

export default class Header extends React.Component {
	constructor(props) {
		super(props);

		this.hamburgerToggle = this.hamburgerToggle.bind(this);
	}
	hamburgerToggle = () => {
		document.querySelector('.hamburger').classList.toggle('is-active');
		document.querySelector('.mm').classList.toggle('active');
	};
	render() {
		const {
			children,
			logo,
			updateLangSpanish,
			updateLangEnglish,
			callText,
			spanish
		} = this.props;
		return (
			<StyledHeader>
				<div className="top-bar-wrap">
					<div className="inner-wrap">
						<div className="language-select">
							<div className="sm-button-wrap">
								<a
									href="#"
									className={spanish ? 'active' : ''}
									onClick={e => updateLangSpanish(e)}
								>
									Español
								</a>
							</div>
							<div className="sm-button-wrap">
								<a
									href="#"
									className={spanish ? '' : 'active'}
									onClick={e => updateLangEnglish(e)}
								>
									English
								</a>
							</div>
						</div>
						<div className="number">
							<div className="number-wrap">
								<h3>
									{callText}:{' '}
									<a href="tel:1-888-240-9962">
										{' '}
										<span>(888) 240-9962</span>
									</a>
								</h3>
							</div>
						</div>
					</div>
				</div>
				<div className="primary-header-wrap">
					<div className="inner-wrap">
						<div className="logo-section">
							<Link to="/">
								{/* <h1>The Law Offices of Sergio J. Siderman, Esq.</h1> */}
								<Img sizes={logo.sizes} />
							</Link>
						</div>
						<div className="link-section">
							<div className="mobile-wrap">
								<button
									className="hamburger hamburger--slider"
									onClick={this.hamburgerToggle}
								>
									<div className="hamburger-box">
										<div className="hamburger-inner" />
									</div>
								</button>
							</div>
							<div className="desktop-links">
								<MenuLinks spanish={spanish} />
							</div>
						</div>
					</div>
				</div>
				<MobileMenuWrap className="mm">
					<div className="inner-wrap">
						<div className="mobile-link-wrap">
							<MenuLinks
								spanish={spanish}
								mobileMenuClose={this.hamburgerToggle}
							/>
						</div>
					</div>
				</MobileMenuWrap>
			</StyledHeader>
		);
	}
}
