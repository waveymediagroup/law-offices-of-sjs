import React, { Component } from 'react';
import Img from 'gatsby-image';
import styled from 'styled-components';
import { TransitionMixin, media } from '../helpers';

const WrapperWrap = styled.div`
	max-width: 90vw;
	margin: 0 auto;
	padding: 40px 0px;

	${media.large`max-width: 55vw; padding: 70px 0px;`};

	p {
		margin-bottom: 20px;
		line-height: 25px;
		text-align: center;
		font-size: 14px;
		font-weight: 300;

		${media.large`font-size: 15px;`};
	}

	&.light-press {
		${media.large`padding: 30px 0px 70px;`};
	}
	&.low-padding {
		padding: 20px 0px;
		${media.large`padding: 30px 0px 30px;`};
	}
`;

const Wrapper = ({ children, sm, image = false, low_pad = false }) => {
	return (
		<WrapperWrap
			className={
				sm == true
					? 'small'
					: '' || image == true
						? 'light-press'
						: '' || low_pad == true
							? 'low-padding'
							: ''
			}
		>
			{children}
		</WrapperWrap>
	);
};

export default Wrapper;
