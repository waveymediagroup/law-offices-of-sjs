import React, { Component } from 'react';
import styled from 'styled-components';

const TitleWrap = styled.div`
	padding: 40px 0px;
	background: #000;
	text-align: center;
	h1 {
		color: #fff;
		margin: 0 auto;

		span {
			font-weight: 100;
			font-size: 35px;
		}
	}
`;

const SmallTitleWrap = styled.div`
	color: #222;
	text-align: center;
	h2 {
		margin: 0px;
	}
`;

export const Title = ({ children }) => {
	return (
		<TitleWrap>
			<h1>{children}</h1>
		</TitleWrap>
	);
};

export const SmallTitle = ({ children }) => {
	return (
		<SmallTitleWrap>
			<h2>{children}</h2>
		</SmallTitleWrap>
	);
};
