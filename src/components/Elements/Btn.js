import React, { Component } from 'react';
import { TransitionMixin, media } from '../../helpers';
import styled from 'styled-components';
import Link from 'gatsby-link';

const BtnClearWrap = styled.button`
	background: transparent;
	border: none;
	a {
		border: 1px solid #fff;
		font-size: 16px;
		background: transparent;
		color: #fff;
		padding: 10px 25px;
		display: block;
		text-decoration: none;
		${TransitionMixin('.25s')};

		&:hover {
			background: #fff;
			color: #222;
		}

		&.invert {
			border: 1px solid #222;
			color: #222;

			&:hover {
				background: #222;
				color: #fff;
			}
		}
	}
`;

export default class BtnClear extends Component {
	render() {
		const { children } = this.props;
		return (
			<BtnClearWrap>
				<Link
					className={this.props.invert == true ? 'invert' : ''}
					to={this.props.linkTo}
				>
					{children}
				</Link>
			</BtnClearWrap>
		);
	}
}
