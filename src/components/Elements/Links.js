import React, { Component } from 'react';
import Link from 'gatsby-link';

export default class MenuLinks extends Component {
	render() {
		const { spanish, mobileMenuClose } = this.props;
		return (
			<ul>
				<li>
					<Link onClick={mobileMenuClose} to="/">
						{spanish !== true ? 'Home' : 'Inicio'}
					</Link>
				</li>
				<li>
					<Link onClick={mobileMenuClose} to="/services">
						{spanish !== true ? 'Services' : 'Servicios'}
					</Link>
				</li>
				<li>
					<Link onClick={mobileMenuClose} to="/about">
						{spanish !== true ? 'About' : 'Acerca De Nosotros'}
					</Link>
				</li>
				<li>
					<Link onClick={mobileMenuClose} to="/success-stories">
						{spanish !== true ? 'Success Stories' : 'Historias de éxito'}
					</Link>
				</li>
				{/* <li>
					<Link onClick={mobileMenuClose} to="/press">
						Press
					</Link>
				</li> */}
				<li>
					<Link onClick={mobileMenuClose} to="/contact">
						{spanish !== true ? 'Contact' : '¡CONTÁCTENOS!'}
					</Link>
				</li>

				<li>
					<a
						target="_blank"
						href="https://www.facebook.com/Law-Offices-Of-Sergio-Siderman-214945552404289/t"
					>
						<i className="fab fa-facebook" />
					</a>
				</li>
				<li>
					<a
						target="_blank"
						href="https://www.instagram.com/lawofficesofsergiosiderman"
					>
						<i className="fab fa-instagram" />
					</a>
				</li>
			</ul>
		);
	}
}
