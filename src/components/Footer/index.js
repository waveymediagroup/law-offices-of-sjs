import React from 'react';
import styled from 'styled-components';
import { media } from '../../helpers';

const StyledFooter = styled.footer`
	.inner-wrap {
		background: #4c6696;
		border-top: 1px solid #fff;
		padding: 20px 0px;
		p {
			color: #fff;
			font-size: 14px;
			font-weight: 300;
			margin: 0px;
			text-align: center;

			span {
				display: block;
				margin-top: 5px;
				${media.medium`display: inline-block;margin-top: 0px;`};
			}
		}
	}
`;

const Footer = ({ children }) => {
	const year = new Date().getFullYear();
	return (
		<StyledFooter>
			<div className="inner-wrap">
				<p>
					&copy; {year} Law Offices of Sergio J. Siderman, Esq.{' '}
					<span>All rights reserved.</span>
				</p>
			</div>
		</StyledFooter>
	);
};

export default Footer;
