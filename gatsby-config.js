module.exports = {
	siteMetadata: {
		title: 'The Law Offices of Sergio J. Siderman',
		description:
			'En las Oficinas Legales de Sergio J. Siderman brindamos la representación legal de la más alta calidad para aquellos que buscan obtener la ciudadanía en los Estados Unidos.',
		siteUrl: 'https://sidermanlaw.com'
	},
	plugins: [
		'gatsby-plugin-styled-components',
		{
			resolve: 'gatsby-source-filesystem',
			options: {
				path: `${__dirname}/src/posts`,
				name: 'posts'
			}
		},
		'gatsby-transformer-remark',
		'gatsby-plugin-react-helmet',
		`gatsby-transformer-sharp`,
		`gatsby-plugin-sharp`,
		`gatsby-plugin-sitemap`,
		{
			resolve: `gatsby-plugin-favicon`,
			options: {
				logo: './src/assets/images/nfavicon.png',
				injectHTML: true,
				icons: {
					android: true,
					appleIcon: true,
					appleStartup: true,
					coast: false,
					favicons: true,
					firefox: true,
					twitter: false,
					yandex: false,
					windows: false
				}
			}
		},
		{
			resolve: 'gatsby-source-filesystem',
			options: {
				name: 'img',
				path: `${__dirname}/src/assets/images`
			}
		},
		{
			resolve: 'gatsby-source-wordpress',
			options: {
				baseUrl: 'sjs.wmgdev001.com',
				protocol: 'http',
				hostingWPCOM: false,
				useACF: true
			}
		},
		{
			resolve: `gatsby-plugin-google-analytics`,
			options: {
				trackingId: 'UA-69276455-9',
				// Puts tracking script in the head instead of the body
				head: true
			}
		}
	]
};
